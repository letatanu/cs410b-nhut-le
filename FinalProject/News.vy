# This contract will be the place to sell the information
# The contract consists of the title, location, and brief description about the information selling
# When the time is over, the owner will send the key, that can be used to descrypt the information, to valid buyers who paid equal or higher than the set price


struct Buyer:
    buyer_address: address
    payment: uint256(wei)
    bought: bool

# the address of person that provide information
owner: address

# the title of the information sold
title: public(string[100])

# the information will exist in a duration
duration: public(timestamp)

 # Briefly description the information
brief_description: public(string[100])

 # The location where the information occurs (zipcode)
location: public(int128)

# the price of information
price: public(uint256(wei))

# limit the number of people buying information
limited_number: public(int128)

# Counting the number of buyers
buyer_count: int128

# storing buyers' address and their payments
buyers: public(map(int128,Buyer))

#next delivery index
next_delivery_index: int128

# encrypted Information
encrypted_information: string[1000]

# constructor
@public
def __init__(_title: string[100], _duration: timedelta, _brief_description: string[100], _location: int128, _price: uint256(wei), _limited_number: int128, _encrypted_information: string[1000]):
    self.owner = msg.sender
    self.title = _title
    self.duration = block.timestamp + _duration
    self.brief_description = _brief_description
    self.location = _location
    self.price = _price
    self.limited_number = _limited_number
    self.encrypted_information = _encrypted_information


# function to register for buying the information
@public
@payable
def buy_information():
    #check if the information is still in valid time
    assert block.timestamp < self.duration
    #check if the number of people wanting to get information is still valid
    assert self.buyer_count <= self.limited_number
    # check if the buyer has not bought it
    bi: int128 = self.buyer_count
    # store the information of the buyer
    self.buyers[bi].payment = msg.value
    self.buyers[bi].bought = True
    self.buyers[bi].buyer_address = msg.sender
    #increasing the number of buyers
    self.buyer_count = bi + 1

# function for a buyer to receive their payment back
@public
def request_withdraw():
    # check if the buyer is not the owner
    assert not msg.sender == self.owner
    #check if the block is still up
    assert block.timestamp < self.duration

    for i in range(100):
        # if their payment does not meet the price, they can receive their payment back
        if self.buyers[i].buyer_address == msg.sender and self.buyers[i].payment < self.price:
            # refunding
            send(msg.sender, self.buyers[i].payment)
            clear(self.buyers[i])

# verify if a given address made a transaction
@public
@constant
def verify_buyer(_buyer: address) -> bool:
    for i in range(100):
        if self.buyers[i].buyer_address == _buyer and self.buyers[i].payment >= self.price:
            return True
    return False

# get the title
@public
@constant
def get_title() -> string[100]:
    return self.title

# get the brief description of the information
@public
@constant
def get_brief_description() -> string[1000]:
    return self.brief_description

# send the private key to descrypt the information to valid buyers
# max 30 people at a time to avoid gas limit issues
@public
def delivery_key(private_key: bytes[32]):
    # check if the time is expired
    assert block.timestamp >= self.duration
    # check the owner
    assert msg.sender == self.owner

    assert self.next_delivery_index <= self.limited_number
    nextI: int128 = self.next_delivery_index
    for i in range(nextI, nextI+30):
        # check if the buyer is valid, their payment is equal or higher than the price.
        if not self.buyers[i].buyer_address == ZERO_ADDRESS and self.buyers[i].payment >= self.price:
            # send decryption key to the valid buyer
            raw_call(self.buyers[i].buyer_address, private_key, outsize=32, gas = msg.gas)
            clear(self.buyers[i])
    self.next_delivery_index = nextI + 30

# destructor
@public
def end_post():
    assert block.timestamp >= self.duration
    assert msg.sender == self.owner
    selfdestruct(self.owner)
