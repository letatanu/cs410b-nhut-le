# This contract will make a statistic about the owner in contract of selling Information
# The buyers who bought the information will give their feedback about the seller
# This will help the seller get trust.

struct Buyer:
    buyer_address: address
    feed_back: string[1000]

contract Information_Seller:
    def verify_buyer(_buyer: address) -> bool: constant
    def get_title() -> string[100]: constant
    def get_brief_description() -> string[1000]: constant


#storing buyers list
owner: address
# the selling contract address that needs to get feed back
news_contract_address: address
# the title of selling contract
title: public(string[100])
# brief description of selling contract
brief_description: public(string[1000])
# storing feedbacks
feedback: public(map(int128, Buyer))

feedback_count: int128

#constructor
@public
def __init__(_news_contract_address: address):
    self.owner = msg.sender
    self.news_contract_address = _news_contract_address
    self.title = Information_Seller(_news_contract_address).get_title()
    self.brief_description = Information_Seller(_news_contract_address).get_brief_description()

# get feedback from buyers
@public
def send_feedback(_feedback: string[1000]):
    # check if msg.sender was the buyer in the selling contract
    assert not msg.sender == self.owner
    assert Information_Seller(self.news_contract_address).verify_buyer(msg.sender)
    # send feedback
    self.feedback[self.feedback_count] = Buyer({buyer_address: msg.sender, feed_back: _feedback})
    self.feedback_count += 1

# display feedback from a specific buyer address
@public
@constant
def get_feedback_from(_address: address) -> string[1000]:
    for i in range(100):
        if self.feedback[i].buyer_address == _address:
            return self.feedback[i].feed_back
    return ''
# destructor
@public
def end():
    assert msg.sender == self.owner
    selfdestruct(self.owner)
