pragma solidity 0.4.24;

contract SlotMachineAttack {
    address public owner;
    constructor() public payable {
        owner = msg.sender;
    }

    function getSeed() external view returns (bytes32) {
        bytes32 entropy2 = keccak256(abi.encodePacked(msg.sender));
        return entropy2;
    }

}
